﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FastSearch.Search.Interface;
using System.IO;
using System.Threading.Tasks;

namespace FastSearch.Search.SearchImpl
{
    public class DirectorySearch : SearchBase
    {
        public DirectorySearch(Action<SearchParams> action, TaskFactory taskFactory, ParcedArgs pArgs)
            : base(action, taskFactory, pArgs) { }

        public override List<Task> StartSearch()
        {
            StartFilesSearch(_pArgs.DirectoryName);

            if (_pArgs.Recursive)
            {
                StartSearchInnerDirectories();
            }
            return Tasks;
        }

        private void StartFilesSearch(string dirName)
        {
            var files = Directory.GetFiles(dirName)
                .Where(s => _pArgs.FileExtentions.Any(e => s.EndsWith(e)))
                .ToList();
            if (files.Count > 0)
            {
                files.ForEach(file =>
                {
                    Tasks.Add(_taskFactory.StartNew(() => _action(new SearchParams() { FileName = file, PArgs = _pArgs })));
                });
            }
        }

        private void StartSearchInnerDirectories()
        {
            var directories = Directory.GetDirectories(_pArgs.DirectoryName);
            if (directories.Count() > 0)
            {
                foreach (var dirName in directories)
                {
                    var ds = new DirectorySearch(_action, _taskFactory, _pArgs);
                    Tasks.AddRange(ds.StartSearch());
                }
            }
        }
    }
}
