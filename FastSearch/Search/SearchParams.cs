﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FastSearch.Search;

namespace FastSearch
{

    public class SearchParams : ISearchParams
    {
        public string FileName { get; set; }
        public ParcedArgs PArgs { get; set; }
        public int StartIndex { get; set; }
    }

    public interface ISearchParams
    {
        string FileName { get; set; }
        ParcedArgs PArgs { get; set; }
        int StartIndex { get; set; }
    }
}
