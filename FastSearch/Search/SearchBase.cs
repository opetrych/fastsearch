﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FastSearch.TPL;
using FastSearch.Search.Interface;

namespace FastSearch.Search
{
    public abstract class SearchBase : ISearch
    {
        protected Action<SearchParams> _action;
        protected TaskFactory _taskFactory;
        protected ParcedArgs _pArgs;

        public SearchBase(Action<SearchParams> action, TaskFactory taskFactory, ParcedArgs pArgs)
        {
            _action = action;
            //if (!Directory.Exists(pArgs.DirectoryName))
            //{
            //    throw new Exception(string.Format("Given Directory '{0}' does not exist", pArgs.DirectoryName));
            //}

            _taskFactory = taskFactory;
            _pArgs = pArgs;

            Tasks = new List<Task>();
        }

        public List<Task> Tasks { get; set; }

        public abstract List<Task> StartSearch();
    }
}
