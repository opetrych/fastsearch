﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastSearch.Search.Matches
{
    public class MatchesResult
    {
        public string FileName { get; set; }
        public int LineNumber { get; set; }
        public int PositionInLine { get; set; }
        public int SymbolInFileIndex { get; set; }
    }
}
