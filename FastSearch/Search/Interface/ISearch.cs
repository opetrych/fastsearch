﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastSearch.Search.Interface
{
    public interface ISearch
    {
        List<Task> StartSearch();
    }
}
