﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastSearch.Search
{
    public class ParcedArgs
    {
        public bool Recursive { get; set; }
        public bool NoLine { get; set; }
        public bool IgnoreCase { get; set; }
        public bool OnlyOne { get; set; }
        public bool ShowLineNum { get; set; }
        public string Pattern { get; set; }
        public List<string> FileExtentions { get; set; }
        public string DirectoryName { get; set; }
        public int MaxDegreeOfParallelism { get; set; }
    }
}
