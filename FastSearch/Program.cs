﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using FastSearch.Search.SearchImpl;
using FastSearch.TPL;
using System.Text.RegularExpressions;
using FastSearch.Search;
using FastSearch.Search.Matches;
using System.Diagnostics;

namespace FastSearch
{
    class Program
    {

        private static List<MatchesResult> _matchesIndexes = new List<MatchesResult>();

        static void Main(string[] args)
        {
            //args = new string[] { "abc", "-r", "-1", ".txt" };

            //args = new string[] { @"d:\Docs\Projs\FastSearch\SourceFolder", "2", "Management", ".txt", "-r", "-1" };

            ParcedArgs pArgs;
            if (ParceArgs(args, out pArgs))
            {
                var search = new DirectorySearch(SearchHandle,
                                                new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(pArgs.MaxDegreeOfParallelism)),
                                                pArgs);
                Task.WaitAll(search.StartSearch().ToArray());

                PrintParameters(pArgs);
                PrintResult();
            }

        }

        private static void PrintParameters(ParcedArgs pArgs)
        {
            var tt = string.Join(",", pArgs.FileExtentions.ToArray());
            //Hack
            var listParms = new List<ParcedArgs>();
            listParms.Add(pArgs);
            Console.WriteLine(listParms.AsEnumerable().ToStringTable(new[] { "Directory", "Pattern", "Max Threads Count", 
                                                                             "File Ext", "Ignore Case", "NoLine",
                                                                              "Catch 1st", "Recursive", "Show line Num"},
                a => a.DirectoryName, a => a.Pattern, a => a.MaxDegreeOfParallelism,
                a => string.Join(",", a.FileExtentions.ToArray()), a => a.IgnoreCase, a => a.NoLine,
                a => a.OnlyOne, a => a.Recursive, a => a.ShowLineNum));
        }

        private static void PrintResult()
        {
            _matchesIndexes.Sort((i1, i2) => i1.FileName.CompareTo(i2.FileName));
            Console.WriteLine(_matchesIndexes.AsEnumerable().ToStringTable(new[] { "File Name", "Line", "Position in Line", "index In File" },
                a => a.FileName, a => a.LineNumber, a => a.PositionInLine, a => a.SymbolInFileIndex));
        }

        public static void SearchHandle(SearchParams sParas)
        {
            Console.Write(string.Format("- {0}{1}", Path.GetFileName(sParas.FileName), Environment.NewLine));
            var pattern = sParas.PArgs.Pattern;
            int currMatchIndex = 0;
            int CurrIndex = 0;
            int CurrLineIndex = 1;
            int CurrIndexInLine = 0;
            var patternLenth = pattern.Length;
            var newLineChar = '\n';
            var fileName = Path.GetFileName(sParas.FileName);
            using (StreamReader reader = new StreamReader(sParas.FileName))
            {
                char fileChar;
                if (sParas.PArgs.IgnoreCase)
                {
                    pattern = pattern.ToUpper();
                }

                do
                {
                    fileChar = sParas.PArgs.IgnoreCase ? Char.ToUpper((char)reader.Read()) : (char)reader.Read();

                    // Compare
                    if (pattern[currMatchIndex].Equals(fileChar))
                    {
                        currMatchIndex++;
                        if (currMatchIndex.Equals(patternLenth))
                        {
                            _matchesIndexes.Add(new MatchesResult()
                            {
                                FileName = fileName,
                                LineNumber = CurrLineIndex,
                                PositionInLine = CurrIndexInLine - (patternLenth - 1),
                                SymbolInFileIndex = CurrIndex - (patternLenth - 1)
                            });
                            currMatchIndex = 0;

                            //if we need at least 1st match
                            if (sParas.PArgs.OnlyOne)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        currMatchIndex = 0;//reset matches
                    }

                    CurrIndex++;
                    CurrIndexInLine++;

                    //Check for new line
                    if (fileChar.Equals(newLineChar))
                    {
                        CurrLineIndex++; //increment to next line
                        CurrIndexInLine = 0;//reset current line counter
                    }
                }
                while (!reader.EndOfStream);
            }
        }

        private static bool ParceArgs(string[] args, out ParcedArgs pArgs)
        {
            pArgs = new ParcedArgs();
            string _usage = "Usage: bgrep [-r] [-i] [-1] \r\n\t-r\trecursive\r\n\t-i\tcase insensitive\r\n\t-1\tonly show the first match in a file\r\n\t\r\n\tSample - d:\\Docs\\Projs\\FastSearch\\SourceFolder 2 Management .txt -r -1\r\n\t";

            if (pArgs.FileExtentions == null)
            {
                pArgs.FileExtentions = new List<string>();
            }

            var skips = new List<string>();
            var skipping = false;
            foreach (var s in args)
            {
                if (string.IsNullOrEmpty(s))
                    continue;

                var ls = s.ToLower();
                if (ls.Equals("-r") || ls.Equals("/r"))
                    pArgs.Recursive = true;
                else if (ls.Equals("-i") || ls.Equals("/i"))
                    pArgs.IgnoreCase = true;
                else if (ls.Equals("-1") || ls.Equals("/1"))
                    pArgs.OnlyOne = true;
                else if (ls.Equals("-s") || ls.Equals("/s"))
                    pArgs.ShowLineNum = true;
                else if (ls.Equals("-n") || ls.Equals("/n"))
                {
                    pArgs.NoLine = true;
                    pArgs.OnlyOne = true;
                }
                else if (ls.Equals("-d") || ls.Equals("/d"))
                    skipping = true;
                else if (skipping)
                    skips.Add(s);
            }

            //Get directory
            if (Directory.Exists(args.GetValue(0).ToString()))
            {
                pArgs.DirectoryName = args.GetValue(0).ToString();
            }

            // Amount of Threads
            int maxDegree = -1;
            if (Int32.TryParse(args.GetValue(1).ToString(), out maxDegree))
            {
                pArgs.MaxDegreeOfParallelism = maxDegree;
            }

            pArgs.Pattern = args.GetValue(2).ToString();

            pArgs.FileExtentions.Add(args.GetValue(3).ToString());

            if (string.IsNullOrEmpty(pArgs.DirectoryName))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Directory was not set.");
                Console.Error.WriteLine(_usage);
                return false;
            }

            if (pArgs.Pattern == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Missing the regular expression text pattern.");
                Console.Error.WriteLine(_usage);
                return false;
            }

            if (pArgs.FileExtentions.Count <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("At least one file search pattern is required. Wildcards are supported.");
                Console.Error.WriteLine(_usage);
                return false;
            }

            Regex regex;
            try
            {
                regex = new Regex(pArgs.Pattern, RegexOptions.Compiled | (pArgs.IgnoreCase ? RegexOptions.IgnoreCase : 0));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("The regular expression provided was invalid. Message: " + ex.Message);
                return false;
            }

            return true;
        }
    }


    public interface ISearchAlgorythm
    {
        //IEnumerable<int> BoyerMooreMatch(string text);
        IEnumerable<int> FindMatches(string text, int startingIndex);
    }

}
